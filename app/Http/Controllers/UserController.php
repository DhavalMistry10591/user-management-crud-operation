<?php

namespace App\Http\Controllers;

use App\Repositories\IUserRepository as IUserRepository;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    public $user;

    public function __construct(IUserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * Get all the users data.
     */
    public function showUsers()
    {
        try {
            $users = $this->user->getAllUsers();
            return View::make('user.index', compact('users'));
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }

    /**
     * Bind the view of add and edit form.
     */
    public function createUser()
    {
        try {
            return View::make('user.edit');
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }

    /**
     * Get user data from the user id.
     */
    public function getUser($id)
    {
        try {
            $user = $this->user->getUserById($id);
            return View::make('user.edit', compact('user'));
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }

    /**
     * Save the user data if $id is not availble then add the user data else update the user data.
     */
    public function saveUser(Request $request, $id = null)
    {
        try {
            $collection = $request->except(['_token', '_method']);
            if (!is_null($id)) {
                $this->user->createOrUpdate($id, $collection);
            } else {
                $this->user->createOrUpdate($id = null, $collection);
            }
            return redirect()->route('user.list');
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }

    /**
     * Delete the user data from $id.
     */
    public function deleteUser($id)
    {
        try {
            $this->user->deleteUser($id);
            return redirect()->route('user.list');
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
        }
    }
}
