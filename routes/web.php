<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [UserController::class, 'showUsers'])->name('user.list');
Route::get('/users', [UserController::class, 'showUsers'])->name('user.list');
Route::get('/user/create', [UserController::class, 'createUser'])->name('user.create');
Route::post('/user/create', [UserController::class, 'saveUser']);
Route::get('/user/edit/{id}', [UserController::class, 'getUser'])->name('user.edit');
Route::put('/user/edit/{id}', [UserController::class, 'saveUser'])->name('user.update');
Route::get('/user/delete/{id}', [UserController::class, 'deleteUser'])->name('user.delete');